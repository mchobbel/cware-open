<?php
/**
 * Plugin Name: Cware test WP-fusion
 * Text Domain: cware-test-wp-fusion
 * Plugin URI: https://gitlab/mchobbel/qware/wordpress/plugins/cware-test-wp-fusion
 * Author: Michiel Hobbelman
 * Author URI: https://gitlab/mchobbel/
 * License: GPLv3
 * License URI: http://www.gnu.org/licenses/gpl.txt
 * Description: Demonstrates a WPF problem
 * Requires PHP: 7.0
 * Requires At Least: 5.8
 * Tested Up To: 5.9
 * Version: 0.0.1
 */

if ( ! defined( 'ABSPATH' ) ) {

	die( 'These aren\'t the droids you\'re looking for.' );
}

class Cware_page{
    const pageid = 'cware_test_wpf_page';

    function show(){
        echo "<h3> Cware demo-page </h3>";
        echo "<pre>";
        if($_GET['gh-contact-id']){
            $ghid = absint($_GET['gh-contact-id']);
        }
        if(isset($_POST['action'])){
            print_r($_POST);
            $contact = new \Groundhogg\Contact($ghid);
            $data = [
                "email" => $_POST['email'],
                "first_name"  => $_POST['first'],
                "last_name"  => $_POST['last'],
            ];
            $contact->update($data);
            return;
        }
        
        echo '<form method="get">';
        echo "<input name=\"gh-contact-id\" placeholder=\"Gh-contact-id\" value=\"{$ghid}\">";
        $pageid = self::pageid;
        echo "<input type=\"hidden\" name=\"page\" value=\"{$pageid}\">";
        echo '<input type="submit" value="Show GH-contact">';

        echo '</form> <hr>';
        


        echo '<form method="post">';
        if($ghid){
            $ghid = absint($_GET['gh-contact-id']);
            $contact = new \Groundhogg\Contact($ghid);
            $data = [
                "gh-contact-id" => $contact->ID,
                "email" => $contact->email,
                "first"  => $contact->first_name,
                "last"  => $contact->last_name,
            ];
            foreach($data as $k=>$v){
                echo "$k : <input name=\"{$k}\" value=\"$v\"> \n";
            }
        }
        echo '<input type="submit" value="Save GH-contact">';
        echo '<input type="hidden" name="action" value="update-gh">';
        echo "</form>";
    }
}


class Cware_test_wpfusion{
    function __construct() {
        add_action('admin_menu', [$this, 'menu_page']);
        add_action('groundhogg/db/post_update/contact', [ $this, 'on_post_update'],10,1);
        add_action('groundhogg/admin/contact/save', [ $this, 'on_admin_save'],10,1);
    }
    function on_post_update($contact){
        error_log("cware-test on-post-update");
    }
    function on_admin_save($contact){
        error_log("cware-test on-admin-save");
    }
    function menu_page(){
        $page1 = new Cware_page();
        add_menu_page(
            __( 'Cware test WPF', 'textdomain' ),
            __( 'Cware test WPF','textdomain' ),
            'read', // capabilities
            Cware_page::pageid,
            [$page1,'show'],
            ''
        );
    }
}

new Cware_test_wpfusion();
